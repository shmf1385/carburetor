# Carburetor

[![Translation status](https://hosted.weblate.org/widgets/carburetor/-/translations/svg-badge.svg)](https://hosted.weblate.org/engage/carburetor/?utm_source=widget)

This is a graphical settings app for tractor which is a package uses Python stem library to provide a connection through the onion proxy and sets up proxy in user session, so you don't have to mess up with TOR on your system anymore. 

## Install
Carburetor is natively packaged for some distros:

[![Packaging status](https://repology.org/badge/vertical-allrepos/carburetor.svg)](https://repology.org/project/carburetor/versions)

If you're on any other distribution or OS, Tractor team welcomes you to make build recepie of yours.

You can also use use global package mangers like [Python Pypi](https://pypi.org/project/carburetor)

## Run
you can run `carburetor` by command line or through your desktop environment.

## Contribute
Contibuting is always appreciated. See [CONTRIBUTING](https://framagit.org/tractor/carburetor/-/blob/main/CONTRIBUTING.md) for more information.

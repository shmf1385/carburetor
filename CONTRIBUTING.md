There are some important areas you can contribute to this project:

## Code
* Replace deprecated `FileChooserNative` with new [FileDialog](https://docs.gtk.org/gtk4/class.FileDialog.html).
* Have better visuals during checking connection.
* Check bridges syntax based on bridge type.
* Use [SwitchRow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.SwitchRow.html) and [SpinRow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.SpinRow.html) in preferences.
* Ideally create `AdwAboutWindow` [using AppStream metadata](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/ctor.AboutWindow.new_from_appdata.html).
* Move SaveButton click logic to actions.

## Community
* Improvement in Pypi release to support desktop-related issues better.
* Making native package build recepies for more GNU/Linux distros.
* Designing a better app icon to suit other GNOME apps.
* Making [appstream](https://www.freedesktop.org/software/appstream/docs/) data correct.
* Releasing on Flathub and moving it to GNOME Circle.

��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
          /  9   @     z     �     �  	   �  #   �     �  
   �     �     �  1   
     <     M     Z     k     {  
   �     �     �     �     �     �     �     �  /   �  2     ,   O  	   |     �     �     �     �  )   �     �     �     �     �     �            ?     	   [  	   e     o     {     �     �     �     �     �  $   �     �  !         "     >     S     X     f     o     {     �     �     �  	   �     �     �     �       ^             7   6       1      F          9       2   =   D   C   *              .                             A      ,             :               -      %                  )   #   @                  8   +   <      
                4   5      &   B   /          ?   >                 0   "   (      !   	      E          '   3   ;   $      G                  H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Portuguese (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <https://hosted.weblate.org/projects/carburetor/translations/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.16.2-dev
 Sobre o Carburetor Aceitar conexão Permitir ou não que os aparelhos externos usem esta rede Áustria Automático (melhor) Pontes Bulgária Não é possível guardar as pontes Canadá Carburetor Plugin de cliente Transport A conectar… Direitos de autor © 2019-2023, Equipa do Tractor República Checa Porta de DNS A desconectar… País da saída Nó de saída Finlândia Para o Tractor França GTK frontend para o Tractor Geral Alemanha Porta de HTTP Irlanda Porta local na qual o Tractor estará à escuta Porta local a qual o túnel HTTP estará à escuta Porta local do servidor de nome anónimo DNS Moldávia Países Baixos Nenhum Noruega Ofuscado Por favor, verifique a sintaxe das pontes Polónia Portas Preferências Sair Roménia A ser executado Rússia Guardar pontes como um ficheiro nas suas configurações locais Seicheles Singapura Porta socks Código-fonte Espanha Parado Suécia Suíça O país de onde quer ligar O Tractor não conseguiu conectar-se Tractor está conectado Tractor não está em execução! Tractor está em execução Tractor está parado Tipo Tipo de ponte Ucrânia Reino Unido Estados Unidos da América Vanilla Tem uma nova identidade! _Verifique a conexão _Conectar _Desconectar _Nova identidade (ID) _Guardar _Alternar o proxy no sistema Manuela Silva <mmsrs@sky.com>
Sérgio Morais <lalocas@protonmail.com>
Ssantos <ssantos@web.de> 
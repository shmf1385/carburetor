��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
          ,  H   >     �     �     �     �      �     �  
   �     �       $        ;     C     Q     b     n  	   }     �     �     �     �     �     �     �  2   �  7     @   G     �     �     �     �     �  $   �     �     �     �     �                  =     
   Z     e     n     ~     �     �     �     �  (   �     �     �          %     A     S     X     g     o     {     �     �     �  
   �     �     �     �      �              7   6       1      F          9       2   =   D   C   *              .                             A      ,             :               -      %                  )   #   @                  8   +   <      
                4   5      &   B   /          ?   >                 0   "   (      !   	      E          '   3   ;   $      G                  H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Spanish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Spanish <https://hosted.weblate.org/projects/carburetor/translations/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 Acerca de Carburetor Aceptar conexión Indica si debe permitirse a los dispositivos externos el uso de esta red Austria Automático (el mejor) Puentes Bulgaria No se pueden guardar los puentes Canadá Carburetor Plugin de transporte Cliente Conectando… Copyright © 2019-2023, Tractor Team Chequia Puerto de DNS Desconectando… Salir País Nodo de salida Finlandia Para Tractor Francia Interfaz GTK de Tractor General Alemania Puerto de HTTP Irlanda Puerto local en el que Tractor estaría escuchando El puerto local en el que se escucharía un túnel HTTP Puerto local en el cual tendrías un servidor de nombre anónimo Moldavia Países Bajos Ninguno Noruega Ofuscado Compruebe la sintaxis de los puentes Polonia Puertos Preferencias Salir Rumania Ejecutándose Rusia Guarde Bridges como un archivo en sus configuraciones locales Seychelles Singapur Puerto de SOCKS Código Fuente España Parado Suecia Suiza El país desde el cual quiere conectarse Tractor no pudo conectarse Tractor está conectado No se está ejecutando Tractor! Tractor está ejecutándose Tractor se detuvo Tipo Tipo de puente Ucrania Reino Unido Estados Unidos Vanilla Tor Tiene una identidad nueva! Verificar conexión _Connectar _Desconectar _Identidad nueva _Guardar _Alternar el proxy en el sistema reconocimiento-traductor 
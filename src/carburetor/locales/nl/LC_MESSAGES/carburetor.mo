��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
          !  3   7  
   k     v     �  	   �  &   �     �  
   �     �     �  #     
   (  	   3  %   =     c     o     }     �  	   �     �     �  	   �  
   �     �  ,   �  ;   	  ;   E  
   �  	   �     �  	   �     �  %   �     �     �  
   �  	   �  	   �             6     
   F  	   Q  
   [     f     o     v     ~     �  ,   �  !   �     �     �               2     8  	   E     O     c  	   t     ~     �  
   �     �     �     �     �              7   6       1      F          9       2   =   D   C   *              .                             A      ,             :               -      %                  )   #   @                  8   +   <      
                4   5      &   B   /          ?   >                 0   "   (      !   	      E          '   3   ;   $      G                  H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Dutch (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Dutch <https://hosted.weblate.org/projects/carburetor/translations/nl/>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.18-dev
 Over Carburetor Verbindingen toestaan Externe apparaten toestaan dit netwerk te gebruiken Oostenrijk Automatisch (beste) Bridges Bulgarije De bridges kunn niet worden opgeslagen Canada Carburetor Cliënttransportplug-in Bezig met verbinden… Copyright © 2019-2023, Tractorteam Tsjechiẽ Dns-poort Bezig met verbreken van verbinding… Afsluitland Afsluitpunten Finland Voor Tractor Frankrijk GTK-frontend voor Tractor Algemeen Duitsland Http-poort Ierland De lokale poort waarop Tractor zal luisteren De lokale poort waarop een http-tunnel zou kunnen luisteren De lokale poort waarop u een anonieme naamserver kan hebben Moldaviẽ Nederland Geen Noorwegen Anoniem Controleer de syntaxis van de bridges Polen Poorten Voorkeuren Afsluiten Roemenië Actief Rusland Sla bridges op als een bestand in uw lokale voorkeuren Seychellen Singapore Sockspoort Broncode Spanje Gestopt Zweden Zwitserland Het land van waaruit u verbinding wilt maken Tractor kan geen verbinding maken Tractor is verbonden Tractor is niet actief! Tractor is actief Tractor is gestopt Soort Soort bridge Oekraïne Verenigd Koninkrijk Verenigde Staten Standaard U heeft een nieuwe identiteit! Verbinding _controleren _Verbinden Ver_binding verbreken _Nieuw identiteitsbewijs Op_slaan Systeemproxy aan/ui_t Philip Goto, Heimen Stoffels 
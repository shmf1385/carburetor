��    9      �  O   �      �     �  *   �     '     /     ;     C     L  
   S     ^     v  $   �     �     �     �  	   �     �     �     �     �  	   �     �                      
   $  "   /     R     Y     _     k     p     x     �  
   �  	   �     �     �     �     �     �  $   �     �     �          
          '     /     H     Z     c     o     w     }     �  �  �     {	  <   �	     �	     �	     �	  	   �	     
  
   

     
     0
  +   >
     j
     q
     z
     �
  	   �
     �
     �
     �
  	   �
     �
  	   �
     �
     �
     �
     �
     �
          "     (     4     =     E     R     X     `     i  	   y  
   �     �  
   �  (   �     �     �     �     �     �               )  	   @     J     _     s     {  K   �     &   4           5         6            "   (                       
   -          ,   9                               	   *             /      '   2               !      7      %          #          .      $   3           1   )      0   +                              8                Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Node Finland France General Germany HTTP Port Ireland Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Seychelles Singapore Source Code Spain Stopped Sweden Switzerland The country you want to connect from Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Polish (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Polish <https://hosted.weblate.org/projects/carburetor/translations/pl/>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.18-dev
 Zaakceptuj połączenia Zezwól urządzeniom zewnętrznym na korzystanie z tej sieci Austria Automatycznie (najlepiej) Mostki Bułgaria Kanada Carburetor Wtyczka transportu klienta Łączenie… Copyright © 2019-2023, Zespół Traktorów Czechy Port DNS Rozłączanie… Węzły wyjściowe Finlandia Francja Ogólne Niemcy Port HTTP Irlandia Mołdawia Holandia Brak Norwegia Zaciemnione Sprawdź składnię mostów Polska Porty Preferencje Zakończ Rumunia Działające Rosja Seszele Singapur Kod źródłowy Hiszpania Zatrzymane Szwecja Szwajcaria Kraj, z którego chcesz się połączyć Rodzaj Rodzaj mostu Ukraina Wielka Brytania Stany Zjednoczone Wanilia Masz nową tożsamość! _Sprawdź połączenie Połą_cz _Zerwij połączenie _Nowy identyfikator _Zapisz _Przełącz proxy w systemie Radek Raczkowski, Adam Hulewicz, Radosław Korotkiewicz, David Troianovskyi 
��    $      <  5   \      0     1  *   D     o     w     �     �  
   �  $   �     �     �  	   �     �     �     �  	   �     �                          '     /  
   6  	   A  
   K     V     \     c  $   o     �     �     �     �     �     �  �  �     w  �   �     '  #   6     Z     m  
   |  D   �     �     �     �               "     3     A     R     c     t     �     �  
   �     �     �     �     �     �     	  K   	     `	     q	  #   �	  %   �	  
   �	     �	                                    	                        $            #   "                                   
                        !                                              Accept Connections Allow external devices to use this network Austria Auto (Best) Bulgaria Canada Carburetor Copyright © 2019-2023, Tractor Team Czech DNS Port Exit Node Finland France Germany HTTP Port Ireland Moldova Netherlands Norway Poland Romania Russia Seychelles Singapore Socks Port Spain Sweden Switzerland The country you want to connect from Ukraine United Kingdom United States You have a new identity! _New ID _Save Project-Id-Version: Greek (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Greek <https://hosted.weblate.org/projects/carburetor/translations/el/>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.18-dev
 Αποδοχή σύνδεσης Πότε επιτρέπεται ή όχι σε εξωτερικές συσκευές να χρησιμοποιούν αυτό το δίκτυο Αυστρία Αυτόματο (καλύτερο) Βουλγαρία Καναδάς Carburetor Πνευματικά δικαιώματα © 2019-2023, Tractor Team Τσεχικά θύρα DNS Κόμβος εξόδου Φινλανδία Γαλλία Γερμανία θύρα HTTP Ιρλανδία Μολδαβία Ολλανδία Νορβηγία Πολωνία Ρουμανία Ρωσία Σεϋχέλλες Σιγκαπούρη θύρα Socks Ισπανία Σουηδία Ελβετία Η χώρα από την οποία θέλετε να συνδεθείτε Ουκρανία Ηνωμένο Βασίλειο Ηνωμένες Πολιτείες Έχετε νέα ταυτότητα! _Νέο ID _Αποθήκευση 
��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
          *  A   B     �     �     �     �  "   �     �  
   �  "   �       '     	   <     F     O     _     n     ~     �     �     �  	   �  	   �  	   �     �  -   �  -   
  >   8     w     �     �     �  	   �     �     �     �     �     �     �     �     �  C     
   H  	   S  
   ]     h     t     |     �     �  4   �  !   �     �                1     F     K     X     `     l     x  #   �     �  
   �     �  
   �     �  "   �              7   6       1      F          9       2   =   D   C   *              .                             A      ,             :               -      %                  )   #   @                  8   +   <      
                4   5      &   B   /          ?   >                 0   "   (      !   	      E          '   3   ;   $      G                  H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: French (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: French <https://hosted.weblate.org/projects/carburetor/translations/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.18-dev
 À propos de Carburetor Accepter des connexions Autoriser ou non les appareils extérieurs à utiliser ce réseau Autriche Auto (recommandé) Ponts Bulgarie Impossible d'enregistrer les ponts Canada Carburetor Extension de transport des clients Connexion… Copyright © 2019-2023, Équipe Tractor Tchéquie Port DNS Déconnexion… Pays de sortie Nœud de sortie Finlande Pour Tractor France Interface GTK pour Tractor Général Allemagne Port HTTP Irlande Port local via lequel Tractor serait écouté Port local via lequel Tractor serait écouté Port local pour un serveur DNS anonyme que vous pourriez avoir Moldavie Pays-Bas Aucun Norvège Obfusqué Veuillez vérifier leur syntaxe Pologne Ports Préférences Quitter Roumanie En exécution Russie Enregistrer les ponts dans un fichier de votre configuration locale Seychelles Singapour Port SOCKS Code source Espagne Arrêté Suède Suisse Le pays depuis lequel vous voulez être connecté·e Tractor n’a pas pu se connecter Tractor est connecté Tractor est à l'arrêt ! Tractor est en marche Tractor est arrêté Type Type de pont Ukraine Royaume-Uni États-Unis Vanille Vous avez une nouvelle identité ! _Vérifier la connexion _Connecter _Déconnecter _Nouvel ID _Enregistrer _Basculer le proxy sur le système – 
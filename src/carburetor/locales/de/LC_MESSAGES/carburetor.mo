��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
          $  @   =     ~     �     �  	   �  ,   �     �  
   �     �     
  $   '  
   L     W     `     }     �     �     �  
   �     �  	   �     �  	   �     �  +   �  :   $  3   _     �     �     �     �  
   �      �     �     �     �     �  	               @   &  
   g     r  
   {  	   �     �  
   �     �     �  9   �  *   �          /     J     c     |     �     �     �     �     �  ,   �     �  
                
   )     4     Q         7   6       1      F          9       2   =   D   C   *              .                             A      ,             :               -      %                  )   #   @                  8   +   <      
                4   5      &   B   /          ?   >                 0   "   (      !   	      E          '   3   ;   $      G                  H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: German (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: German <https://hosted.weblate.org/projects/carburetor/translations/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.18-dev
 Über Carburetor Verbindungen Akzeptieren Ermöglichen Sie externen Geräten, dieses Netzwerk zu verwenden Österreich Automatisch (Optimal) Brücken Bulgarien Die Bridges konnten nicht gespeichert werden Kanada Carburetor Klient Transport Plugin Verbindung wird erstellt … Copyright © 2019-2023, Tractor Team Tschechien DNS-Port Verbindung wird getrennt … Ausgang Land Exit-Knoten Finnland Für Tractor Frankreich GTK Frontend für Tractor Allgemein Deutschland HTTP-Port Irland Lokaler Port, auf dem Tractor lauschen soll Lokaler Port, an dem ein HTTP-Tunnel abgehört werden soll Lokaler Port, auf dem der anonyme DNS-Server läuft Moldau Niederlande Kein Norwegen Obfuskiert Bitte Syntax der Bridges prüfen Polen Ports Einstellungen Beenden Rumänien Wird ausgeführt Russland Speichern Sie Bridges als Datei in Ihren lokalen Konfigurationen Seychellen Singapur Socks-Port Quellcode Spanien Angehalten Schweden Schweiz Das Land, aus dem Sie eine Verbindung herstellen möchten Traktor konnte keine Verbindung herstellen Tractor ist verbunden Tractor ist ausgeschaltet! Tractor wird ausgeführt Tractor wurde angehalten Typ Art der Brücke Ukraine Vereinigtes Königreich Vereinigte Staaten Vanille Ihnen wurde eine neue Identität zugeordnet! _Verbindung prüfen _Verbinden _Trennen _Neue ID _Speichern Proxy auf System umschal_ten Übersetzer Anerkennungen 
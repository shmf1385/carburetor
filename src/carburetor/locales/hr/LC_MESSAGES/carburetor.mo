��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
     c     u  0   �     �     �     �     �     �       
   	          3  .   C     r     z     �     �     �     �  
   �  	   �     �     �  	   �     �       4     7   J  D   �  	   �  
   �     �  	   �  	   �     �               %     .  	   >  	   H     R  6   Y     �     �     �     �     �  	   �     �  
   �  "   �          /     B     Y     m     �     �     �     �     �     �     �     �     �  
   �                    7         7   6       1      F          9       2   =   D   C   *              .                             A      ,             :               -      %                  )   #   @                  8   +   <      
                4   5      &   B   /          ?   >                 0   "   (      !   	      E          '   3   ;   $      G                  H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Croatian (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Croatian <https://hosted.weblate.org/projects/carburetor/translations/hr/>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.16.2-dev
 Carburetor podaci Prihvati veze Dopusti vanjskim uređajima koristiti ovu mrežu Austrija Automatski (najbolje) Mostovi Bugarska Nije moguće spremiti mostove Kanada Carburetor Dodatak klijentskog transporta Povezivanje … Autorska prava © 2019. – 2023., Tractor tim Češka Priključak za DNS Odspajanje … Zemlja izlaza Čvor izlaza Finska Za Tractor Francuska GTK sučelje za Tractor Opće Njemačka Priključak za HTTP Irska Lokalni priključak na kojem se Tractor prisluškuje Lokalni priključak na kojem se HTTP tunel prisluškuje Lokalni priključak na kojem se može nalaziti anonimni poslužitelj Moldavija Nizozemska Ništa Norveška Pomućeno Provjeri sintaksu mostova Poljska Priključci Postavke Zatvori program Rumunjska Pokrenuto Rusija Spremi mostove kao datoteku u lokalnim konfiguracijama Sejšeli Singapur Priključak za Socks Izvorni kod Španjolska Prekinuto Švedska Švicarska Zemlja iz koje se želiš povezati Tractor se nije uspio povezati Tractor je povezan Tractor nije pokrenut! Tractor je pokrenut Tractor je prekinut Vrsta Vrsta mosta Ukrajina Ujedinjeno Kraljevstvo Sjedinjene Države Vanilla Imaš novi identitet! _Provjeri vezu _Poveži _Odspojeno _Novi ID _Spremi _Uklj./Isklj. proxy na sustavu Milo Ivir <mail@milotype.de> 
��    H      \  a   �            !     2  *   E     p     x     �     �     �     �  
   �     �     �  $   �                    +  	   8     B     J     V     ]     v     ~  	   �     �  +   �  1   �  ;   �     2     :     F     K  
   R  "   ]     �     �     �     �     �     �     �  ,   �  
   �  	   �  
   �     	     	     	     	     #	  $   /	     T	     m	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     ,
     5
     A
     I
     O
     g
  �  z
     -     ;  A   L  
   �     �     �     �     �     �  
   �     �     �  )   
     4     =     F     U     b     n     v  	   �     �     �     �  	   �     �  !   �  !   �  +        ;  	   C     M     S  	   Y     c     ~     �     �     �     �     �     �  (   �     �  	   �  
   �  	                       %  "   ,     O     l     �     �     �     �     �     �     �     �     �            
   0  
   ;     F     M     T  (   t         7   6       1      F          9       2   =   D   C   *              .                             A      ,             :               -      %                  )   #   @                  8   +   <      
                4   5      &   B   /          ?   >                 0   "   (      !   	      E          '   3   ;   $      G                  H    About Carburetor Accept Connections Allow external devices to use this network Austria Auto (Best) Bridges Bulgaria Can not save the bridges Canada Carburetor Client Transport Plugin Connecting… Copyright © 2019-2023, Tractor Team Czech DNS Port Disconnecting… Exit Country Exit Node Finland For Tractor France GTK frontend for Tractor General Germany HTTP Port Ireland Local port on which Tractor would be listen Local port on which a HTTP tunnel would be listen Local port on which you would have an anonymous name server Moldova Netherlands None Norway Obfuscated Please check the syntax of bridges Poland Ports Preferences Quit Romania Running Russia Save Bridges as a file in your local configs Seychelles Singapore Socks Port Source Code Spain Stopped Sweden Switzerland The country you want to connect from Tractor couldn't connect Tractor is connected Tractor is not running! Tractor is running Tractor is stopped Type Type of bridge Ukraine United Kingdom United States Vanilla You have a new identity! _Check connection _Connect _Disconnect _New ID _Save _Toggle proxy on system translator-credits Project-Id-Version: Norwegian Bokmål (Carburetor)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/carburetor/translations/nb_NO/>
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.18.1
 Om Carburetor Godta tilkobling Hvorvidt eksterne enheter skal tillates å bruke dette nettverket Østerrike Auto (best) Broer Bulgaria Kan ikke lagre broene Canada Carburetor Klienttransport-programtillegg Kobler til … Opphavsrett © 2019–2023, Tractor-laget Tsjekkia DNS-port Kobler fra … Avslutt land Utgangsnode Finland For Tractor Frankrike GTK-skjermflate for Tractor Generelt Tyskland HTTP-port Irland Lokal port Tractor skal lytte til Lokal port Tractor skal lytte til Lokal port du har en anonym navnetjener på Moldova Nederland Ingen Norge Tilslørt Sjekk syntaksen for broene Polen Porter Innstillinger Avslutt Romania Kjører Russland Lagre broer i en fil i ditt lokaloppsett Seychellene Singapore SOCKS-port Kildekode Spania Stoppet Sverige Sveits Landet du ønsker å koble til fra Tractor kunne ikke koble til Traktoren er tilkoblet Tractor kjører ikke! Tractor kjører Tractor er stoppet Type Brotype Ukraina Storbritannia Amerikas forente stater Uten endringer Du har en ny identitet! _Sjekk tilkobling _Koble til _Koble fra _Ny ID _Lagre _Slå mellomtjener på systemet Allan Nordhøy, <epost@anotheragency.no> 